Package.describe({
	summary: "A semantic, fluid grid framework on top of Sass and Bourbon"
});

Package.on_use(function(api) {
	api.use('bourbon');
});
